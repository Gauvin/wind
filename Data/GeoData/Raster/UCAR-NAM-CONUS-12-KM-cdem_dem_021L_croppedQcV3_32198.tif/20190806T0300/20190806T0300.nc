CDF       
      time      height_above_ground1      y         x         height_above_ground2             Originating_or_generating_Center      QUS National Weather Service, National Centres for Environmental Prediction (NCEP)      #Originating_or_generating_Subcenter       0      GRIB_table_version        2,1    Type_of_generating_process        Forecast   PAnalysis_or_forecast_generating_process_identifier_defined_by_originating_centre       MESO NAM Model (currently 12 km)   Conventions       CF-1.6     history       %Read using CDM IOSP GribCollection v3      featureType       GRID   History       �Translated to CF-1.0 Conventions by Netcdf-Java CDM (CFGridWriter2)
Original Dataset = /data/ldm/pub/native/grid/NCEP/NAM/CONUS_12km/NAM-CONUS_12km-noaaport.ncx3; Translation Date = 2019-08-06T00:28:11.315Z     geospatial_lat_min        @F�|�kt-   geospatial_lat_max        @H]���s   geospatial_lon_min        �R5�}���   geospatial_lon_max        �QfF�P"x         Temperature_height_above_ground                          	long_name         1Temperature @ Specified height level above ground      units         K      abbreviation      TMP    missing_value         �     grid_mapping      LambertConformal_Projection    coordinates       &reftime time height_above_ground1 y x      Grib_Variable_Id      VAR_0-0-0_L103     Grib2_Parameter                      Grib2_Parameter_Discipline        Meteorological products    Grib2_Parameter_Category      Temperature    Grib2_Parameter_Name      Temperature    Grib2_Level_Type         g   Grib2_Level_Desc      #Specified height level above ground    Grib2_Generating_Process_Type         Forecast     �  �   reftime                 standard_name         forecast_reference_time    	long_name         GRIB reference time    calendar      proleptic_gregorian    units         Hour since 2019-07-06T00:00:00Z    _CoordinateAxisType       RunTime         $$   time                units         Hour since 2019-07-06T00:00:00Z    standard_name         time   	long_name         !GRIB forecast or observation time      calendar      proleptic_gregorian    _CoordinateAxisType       Time        $4   height_above_ground1               units         m      	long_name         #Specified height level above ground    positive      up     Grib_level_type          g   datum         ground     _CoordinateAxisType       Height     _CoordinateZisPositive        up          $D   y                  standard_name         projection_y_coordinate    units         km     _CoordinateAxisType       GeoY      `  $H   x                  standard_name         projection_x_coordinate    units         km     _CoordinateAxisType       GeoX      H  $�   LambertConformal_Projection              grid_mapping_name         lambert_conformal_conic    latitude_of_projection_origin         @9         longitude_of_central_meridian         @p�        standard_parallel         @9         earth_radius      AXM�@      _CoordinateTransformType      
Projection     _CoordinateAxisTypes      	GeoX GeoY           $�   'v-component_of_wind_height_above_ground                          	long_name         9v-component of wind @ Specified height level above ground      units         m/s    abbreviation      VGRD   missing_value         �     grid_mapping      LambertConformal_Projection    coordinates       &reftime time height_above_ground2 y x      Grib_Variable_Id      VAR_0-2-3_L103     Grib2_Parameter                    Grib2_Parameter_Discipline        Meteorological products    Grib2_Parameter_Category      Momentum   Grib2_Parameter_Name      v-component of wind    Grib2_Level_Type         g   Grib2_Level_Desc      #Specified height level above ground    Grib2_Generating_Process_Type         Forecast     �  $�   height_above_ground2               units         m      	long_name         #Specified height level above ground    positive      up     Grib_level_type          g   datum         ground     _CoordinateAxisType       Height     _CoordinateZisPositive        up          2t   'u-component_of_wind_height_above_ground                          	long_name         9u-component of wind @ Specified height level above ground      units         m/s    abbreviation      UGRD   missing_value         �     grid_mapping      LambertConformal_Projection    coordinates       &reftime time height_above_ground2 y x      Grib_Variable_Id      VAR_0-2-2_L103     Grib2_Parameter                    Grib2_Parameter_Discipline        Meteorological products    Grib2_Parameter_Category      Momentum   Grib2_Parameter_Name      u-component of wind    Grib2_Level_Type         g   Grib2_Level_Desc      #Specified height level above ground    Grib2_Generating_Process_Type         Forecast     �  2x   0Total_cloud_cover_entire_atmosphere_single_layer                      	long_name         +Total cloud cover @ Entire atmosphere layer    units         %      abbreviation      TCDC   missing_value         �     grid_mapping      LambertConformal_Projection    coordinates       reftime time y x       Grib_Variable_Id      VAR_0-6-1_L200     Grib2_Parameter                    Grib2_Parameter_Discipline        Meteorological products    Grib2_Parameter_Category      Cloud      Grib2_Parameter_Name      Total cloud cover      Grib2_Level_Type         �   Grib2_Level_Desc      Entire atmosphere layer    Grib2_Generating_Process_Type         Forecast     �  ?�C��fC�%C��3C��C�~�C��C�q�C�'�C��\C���C���C�p�C�@ C��C�\C�RC�!HC�%C��
C�W
C���C�C�C�� C��\C�g�C�8RC���C��fC���C��fC�aHC�<)C�.C�#�C�\C�)C�� C�AHC�y�C��C���C�,�C��C�.C���C���C��=C��
C�y�C�` C�P�C�:�C��C��C��C�NC�J=C�z�C�P�C�.C�:�C�'�C�޸C���C��RC���C��=C��fC�}qC�` C�:�C�AHC��)C�S3C�Q�C�5�C�9�C��{C��HC�EC��=C��C���C��C���C�NC�� C��=C�U�C�=qC�u�C�k�C�k�C��C�>�C�"�C��RC��HC�qC�C���C���C���C�=qC���C��HC�b�C�P�C��C�s3C�S3C�}qC�t{C�S3C���C�nC�nC�  C���C��\C���C��C�˅C��C�o\C�s3C�HC�w
C��C���C�.C��=C�O\C��\C�
=C��3C���C���C��=C��3C���C���C��fC��\C�|)C�qC��C�7
C���C��RC��=C�b�C�9�C��fC�qC��\C��C���C�޸C��{C��RC��C�7
C���C�ffC�B�C�eC�|)C��C���C�*=C�j=C�*=C�/\C�,�C���C�eC�XRC�^�C�z�C�ФC��HC���C��=C�k�C�33C�{C�%C��HC�  C��C�8RC�5�C�� C�U�C�C�C�g�C���C�ǮC��
C��
C��{C�]qC�  C���C���C��fC���C��{C�L�C�]qC��C�w
C�EC�}qC���C��)C��C�]qC�#�C��C�ٚC�fC���C���C�t{C��3C�3C�7
C�J=C��3C�z�C��fC��)C���C���C�:�C�ФC�p�C�g�C��C���C��)C��qC��=C��{C��RC�` C��C���C���C��fC���C��HC�.C�˅C�` C�3C��RC��C�S3C���C��
C��C�NC�:�C�s3C�#�C���C��\C���C�h�C�  C��{C�q�C��C��{C��
C���C���C�nC��qC���C�/\C���C��C��C��C���C�k�C�  C��{C���C�33C�˅C���C�k�C��\C�>�C��
C�H�C�>�C�=qC���C��{C�33C���C�J=C�\C���C�y�C�*=C���C��HC�\)C�l�C��C��C���C�C��C�^�C��
C�aHC�k�C�4{C���C���C�Z�C��C��C�h�C�FfC�j=C��C��C��
C��qC��\C��C���C�xRC�Q�C�3C���C���C�G�C��=C���C�W
C�T{C���C�@ C�  C�8RC��)C�AHC�<)C�O\C�k�C�FfC��C���C��C�XRC���C��{C�y�C�q�C���C�33C��3C��{C��\C��
C�&fC���C�P�C�Y�C�'�C��C���C�~�C��C��
C��C��HC��=C�.C�� C��fC��C��C�W
C��C�1�C�^�C�8RC�3C��HC���C�7
C��fC��\C���C��C�.C�~�C���C��=C���C��C��
C�HC�xRC�O\C�(�C��3C��fC�t{C�U�C�@ C�G�C�]qC�y�C���C��qC��C�,�C�'�C��\C��
C�T{C�� C��=C���C��C�y�C��C�ФC���C��C���C�B�C��=C�� C�p�C�U�C�NC�NC�J=C��3C�B�C��C���C�&fC�(�C�\C�� C�z�C�k�C�FfC�  C��RC���C�P�C�'�C�<)C�Q�C��RC��3C�33C���C�y�C�` C��C���C�nC�` C�T{C�qC��HC���C�b�C�+�C��C���C��\C���C��=C���C�C�C��C��C���C�XRC�9�C�<)C�.C�)C���C��HC�g�C�J=C�(�C��qC���C��qC��)C���C�z�C�1�C��RC�Q�C��C�)C��C���C�&fC��3C���C�]qC�%C�U�C��3C�|)C���C��HC�˅C���C�RC���C�33C�  C��C�w
C���C�fC���C�|)C�� C�*=C���C��{C�
C�0�C�޸C��C�8RC��\C���C�FfC��C��qC��C��C��C��3C�qC��HC�\)C�!HC���C�C�C���C�ffC��fC�L�C���C�qC��{C���C��C��C��RC��qC��3C�b�C�4{C��HC��HC�5�C��{C���C��)C�޸C��
C�(�C�W
C��C�HC��RC��{C��3C��C���C�z�C�y�C�>�C�{C���C�B�C���C�j=C��fC�B�C��
C�  C���C���C��qC��C�)C��C��C���C��C��fC�T{C�w
C���C��C��C�S3C��)C�w
C�RC��fC��{C��C�,�C�3C��C���C���C��HC�9�C�33C��fC��=C�<)C���C��C���C�Z�C��C��C��C�'�C�C��
C��{C�b�C�FfC�U�C��\C��\C��3C���C��\C��C�b�C��fC�Z�C�qC�HC�!HC��{C��qC�|)C�7
C�fC��RC��RC�qC�O\C��C���C�XRC���C�#�C���C�L�C��C�)C��C��=C�j=C�\C��C���C��)C��C�4{C�\)C�u�C�HC�K�C�|)C��C�nC�0�C��C��RC���C�g�C�)C��3C�b�C�t{C�p�C���C�G�C���C��fC��C�nC��RC�ǮC�u�C�
=C��C��=C�l�C�*=C���C��3C�G�C�B�C���C��C���C��{C��)C��C���C�O\C��)C���C���C��C�w
C�4{C��\C��qC�O\C�5�C�s3C��C��{C�
C��=C��C�eC���C��C��
C�ٚC��{C�}qC�(�C��RC���C�J=C�,�C�p�C��\C���C�P�C��3C�� C���C���C�S3C���C���C��HC�o\C�)C�ФC���C�AHC�O\C���C��C���C��C�}qC�,�C�AHC�*=C�eC��qC��)C��3C�aHC��C�ǮC�xRC�U�C�p�C���C�C��)C��fC���C���C�3C��qC��C��C���C��RC�ffC�%C��RC��RC�� C���C��RC��C�nC���C��{C���C�{C�  C�^�C��RC��)C��C���C�NC�
=C��RC��RC��C��=C��C�P�C��C�t{C�|)C�<)C��C�T{C���C�C���C���C�xRC�U�C�@ C�&fC�G�C�\)C�c�C�g�C�nC�u�C���C��C���C��3@�     @�     @�X     @�p     @   E}<E@JEYE�gE �uE!L�E"�E"ҠE#��E$X�E%�E%��E&��E'd�E((E(�E)�!E*q0E+4>E+�LE,�[E-}iE.@wE/�D�MD�&jD鬆D�2�D��D�>�D���D�KD��2D�WOD��lD�c�D��D�o�D���D�{�D�E D    @QG�@?\)@(Q�@G�@ ��?�=q?�{?�  ?�G�?�33?�(�?�(�?�Q�?�\)?�G�?���?��?Y��@W�@Dz�@0  @�R@p�?�(�?�(�?�ff?\?У�?�(�?�  ?�p�?�?Ǯ?��?�Q�?�G�@W�@Fff@4z�@*�H@�H@Q�?�?��?��
?�=q?�z�?�p�?�G�?��H?���?�Q�?�  ?�=q@Tz�@K�@<(�@1G�@#33@�?��H?��H?���?�ff?���?�
=?޸R?�p�?��?�  ?�ff?���@J=q@Mp�@E�@9��@(��@=q@(�?��R?��
?˅?Ǯ?У�?ٙ�?��H?�{?�G�?�{?�
=@:�H@Dz�@G
=@A�@0  @"�\@��@  @�\?��?�
=?�33?�33?У�?˅?��?�
=?�  @#�
@/\)@9��@;�@1G�@)��@#33@p�@�\@z�?�\)?�
=?��?�G�?��
?�ff?�G�?��@
=q@G�@�@   @#33@(Q�@%@!G�@�H@�\@z�?��H?�p�?�z�?�Q�?��R?�  ?��?���?�{?��?�@G�@{@z�@
=@�H@��@�R?�?�  ?�{?���?���?�33?���?��H?Ǯ?��?�G�?�ff?��?�  ?�p�@{@�@�?�(�?��?�?�{?�=q?��?��\?ٙ�?�{?z�H?#�
?!G�?J=q?���?�z�?�\@�\@	��@ff?��?�33?�p�?�\)?���?��\?�?���?Tz�>�Q�>.{>8Q�>Ǯ?B�\?�\)?�(�?���@��@33?�\)?�33?��H?�{?��@��?�(�?���?   =��
���#�
>��>�(�?Q�?�Q�?�
=@
=@ ��?��
?Ǯ?�33?��@+�@��?�(�?���>�=�Q콸Q�L�;�>���?��?޸R@p�@{?���?�
=?�
=?��\@C33@4z�@{?��R?�=q?B�\>��R�#�
�L��>W
=?k�?�  @p�@#�
@z�?��?�p�?��@U�@QG�@HQ�@333@�?�?���?(�>�p�?�?���?�p�@.�R@AG�@5@�R?��H?�z�@b�\@dz�@aG�@U�@@  @#33?�p�?Ǯ?�ff?�(�?�Q�@p�@AG�@_\)@W
=@(Q�@�\?��@mp�@p  @p  @i��@]p�@J=q@1�@�@	��?��?���@��@<(�@p  @b�\@@��@�H?���@xQ�@|��@}p�@z=q@q�@dz�@U�@B�\@,��@z�?��H?��@��@Mp�@Z�H@P��@5�@@��
@��R@�  @�
=@��
@~{@p��@\(�@AG�@ ��?��H?�Q�?�@p�@E�@W
=@Dz�@(��@��@���@�33@��H@�  @�33@��
@o\)@Q�@.{@   ?�(�?\?��H@/\)@O\)@N{@7�@�z�@���@�(�@��@�33@�{@�p�@�G�@e�@B�\@Q�?�?�\)?��
@#33@K�@\��@L��@�  @�p�@�=q@�z�@�33@�{@�ff@�(�@{�@Z=q@8��@=q?��H?�p�@\)@HQ�@hQ�@Vff@�Q�@��@�p�@�Q�@�\)@�G�@���@��@��H@j=q@QG�@:=q@Q�@Q�@�@9��@]p�@XQ�@E�@<(�@*=q@�H@�\@G�?�p�?�  ?ٙ�?��?��R?��H?��?�  ?�{?�Q�?�ff?�
=@H��@>{@,��@{@Q�@ff@��?���?��?��?��?�33?���?�p�?���?�
=?�ff?�p�@E@;�@.�R@'
=@"�\@p�@��?�33?�p�?޸R?�ff?��?�z�?�=q?�z�?�(�?���?�  @?\)@>{@8Q�@5�@/\)@(Q�@\)@Q�?�z�?�\?�p�?�?�?�z�?�ff?�=q?�z�?�ff@1�@>{@C33@A�@:=q@4z�@.{@ ��@\)?�Q�?�?�?���?�\)?�?�
=?\?���@!G�@1G�@>�R@E@@  @:�H@8��@5�@*=q@�
@�?�z�?�\?�  ?�\?޸R?�33?��R@
=@�H@'�@5@6ff@7
=@8��@=p�@:=q@+�@��@�
?޸R?�z�?ٙ�?��
?�\?��@
=q?�p�@33@�R@��@'
=@.{@5@9��@7
=@,��@�?�{?�z�?�Q�?�?�?�\?�Q�?���?�Q�?��H?У�?�Q�@�R@�R@.{@9��@9��@#33@�?��?��
?�{?�33?��?�Q�?�
=?u?0��?:�H?�G�?�33?�\)@Q�@2�\@:�H@1�@p�@ff@ ��?�p�?�(�?�(�@(�?���?Y��>L��=��
>.{>��H?���?�G�@z�@.�R@;�@2�\@   @�\@	��@z�@�\@{?�?���>��������R��  >u?fff?�=q@z�@7�@:=q@0��@   @33@�@�@333@�H?��?Tz�>�����
���H��p�=L��?5?�z�@��@5@7
=@(��@�@  @�@I��@7
=@�?��?W
=>W
=�B�\��G�����>B�\?�\)?��R@,(�@:=q@.{@   @��@�
@XQ�@QG�@@  @"�\?�G�?��>�=L�ͽ��
>\?���?��@*=q@;�@7�@&ff@G�@33@dz�@e�@a�@N{@)��?�p�?�=q?p��?\(�?���?�(�@z�@,(�@C33@E�@0  @Q�@
=@mp�@r�\@r�\@g
=@Q�@6ff@�@   ?�Q�?�
=?�
=@�
@3�
@N{@S33@8��@"�\@  @tz�@z=q@{�@u@h��@W�@C�
@7
=@0��@%�@33@
=@0��@U@U�@Dz�@0  @\)@z�H@�Q�@���@�Q�@xQ�@mp�@c�
@Z�H@P��@8Q�@=q@Q�@@=p�@L��@N{@A�@0  @���@���@��R@�{@��
@���@z=q@p��@^{@>{@
=?�G�?�\)@@<(�@U�@O\)@@  @�\)@��H@�@�p�@���@��\@�{@~�R@fff@C33@?�(�?˅?�33@.{@U@^{@O\)@�{@�=q@�{@��@�\)@�p�@�  @�ff@qG�@N{@!�?�Q�?�33?��@+�@^{@w�@mp�@�(�@�G�@�{@�=q@��H@�\)@�Q�@�@~�R@\��@9��@��@ ��@��@9��@l(�@�G�@�G�@���@�\)@��@�G�@���@�z�@��
@���@�33@h��@P  @<(�@&ff@(��@E�@u�@��
@���A   ��
=������녿�\)����
=���ÿp�׿W
=�E��+����H��=q��>u>�?333?aG���=q���Ϳ�녿��R���׿��Ϳ�
=�k��8Q�+��z��G���\)��G�=���>��
?\)?J=q��=q��녿�������=q����녿Tz�(�ÿ녿   �Ǯ��z�L�ͽ�\)>��>�ff?(�ÿ�녿�p����׿�ff��G����\�u�333�녾���Q쾙���������R�u=#�
>�p�?
=�}p���{���H��p���33�z�H�J=q�&ff��\��{�aG��W
=���
��(���Q켣�
>�{?���333�L�ͿaG��aG��L�Ϳ:�H�0�׿�R����=q�8Q�aG��\��׾�{��>���?!G��\)�(��.{�333�+��#�
����
=q�Ǯ��zᾣ�
���;���ff��\)    >�{?
=�#�
�=p��G��=p��&ff�
=q�   ����G���\�#�
�.{�(���G��k�<�>���?\)�B�\�n{�}p��s33�\(��.{�z�(��5�c�
��  �fff�333��녽�>�>�Q�?�ͿL�Ϳu��{���H��(���\)��  �xQ쿈�ÿ�p���(�����:�H���R=#�
>���>�?z�\(��}p���33���ÿ��Ϳ�ff���
���\������׿�����Ϳ.{�8Q�>L��>�?
=?+���G����Ϳ�(�������
��  ���R�����=q������R����#�
�#�
>�\)?��?(��?:�H���׿��H��  ��G����H��
=��녿�녿���xQ�z�H�^�R��R���>u?�?.{?:�H��녿�
=��z῏\)������
�c�
�8Q���������þ�
=��
=��  >��>��?.{?@  ���ÿ�ff�xQ�\(��J=q�(�þ�(��u>�{?�?�>�(�>�=�\)>L��>�G�?@  ?J=q�xQ�c�
�@  �z��녾W
==��
?�?�  ?��
?���?��\?W
=>�>��
?�?Tz�?p�׿^�R�E��
=�\���>\)>��H?xQ�?�Q�?��
?��?�z�?�(�?(��>\?(��?p��?�녿E��+��   ��z�#�
>�{?J=q?�ff?�@
=?��R?���?���?��>�G�?(��?z�H?��\�+��z��ff�u=��
?   ?�  ?��
@ ��@�?�=q?�G�?#�
>��
>�33?��?W
=?�Q�
=q��׾�p��\)>k�?.{?��H?�Q�?��R?��H?Ǯ?c�
>\>B�\>�\)>���?(�?u�\���þW
==L��>�
=?\(�?��?��H?�\)?�G�?��?0��>��
>u>�Q�>�Q�>�?333�L�;��    >��?(�?�G�?���?У�?�
=?��?��?!G�>�Q�>�p�>��H>�ff>�G�?���<�>B�\>�G�?J=q?��?�33?��
?��H?�(�?c�
?z�>�p�>�ff?�?��>�
=>��=���>8Q�>�33?�R?xQ�?�  ?�z�?��?�Q�?c�
?(�>\>�=q>�\)>�{>Ǯ>�=q>�  �Q녿h�ÿz�H���ÿ�
=������녿��
�h�ÿQ녿8Q��R��\�Ǯ�W
=<#�
>W
=>Ǯ�&ff�L�Ϳz�H���H��
=�����
���
�O\)�@  �0�׿�R�녿
=q��G����    >�=q�녿Q녿�33�Ǯ��
=�Ǯ����xQ�E��+���������
=�
=��
=��Q�>W
=�\)�Y�����H��ff��\)�����׿^�R�8Q�
=��ff�\��׿���+����H��>#�
���Ϳ0�׿xQ쿐�׿�zῇ��fff�G��.{��\��{���R��׿(�ÿ8Q���H���>��#�
�\�\)�(�ÿ0�׿333�:�H�:�H�!G���
=���R��Q��Ϳ8Q�333�����>\)<��
�����zᾳ33��G��
=q�#�
�.{�
=q��(���ff�녿8Q�@  ��R�\���
>#�
�#�
�L�;��R���þ�Q��(��\)��R��R�(�ÿJ=q�fff�fff�=p����H�L��=#�
>W
=�L�;�G��\)�z��;����!G��J=q���
����녿xQ�(�þ������
>8Q�>�=q�Ǯ��R�Q녿n{�z�H�Y���=p��@  �p�׿�(�������H�p�׿���>�>�\)>��R����@  �k����׿�
=����s33�c�
�z�H���׿�
=��=q�O\)�Ǯ���
>k�>�{>��ÿQ녿aG��p�׿�ff���ÿ�G��n{�^�R�G��=p��@  �@  ��;��=L��>��>���>��
���\��  �u�p�׿fff�W
=�333�������\)��Q�aG�����B�\<�>B�\>�=q>�=q��녿���h�ÿJ=q�5�\)��=q=���>��?0��?�R>\=�\)���L��=u>.{>8Q쿔z῁G��J=q��;�p��#�
>#�
?�R?��?�{?�{?�=q?�=��ͽ�Q콸Q�=u=u��\)�p�׿.{��p���G�>��>�?}p�?��?�
=@�?�G�?��>�p����;aG��u�#�
���ÿfff�(���\)=#�
>�p�?B�\?���?��R@=q@��@33?�\)?\)���
�W
=�#�
�u��  �Tz�녾��=�Q�?   ?�ff?�p�@Q�@-p�@   ?��R?���?#�
>���G��8Q콣�
�k��@  �
=q���=�?�R?��\@ ��@)��@0  @�?��H?��\?z�>�\)=L�ͽ����
�Q녿0�׿�\��  >.{?=p�?���@{@-p�@,(�@��?�?Tz�?�>��>W
=    =u�8Q�����ff�8Q�>�=q?^�R?���@33@)��@%@�?��?\(�?(��?(��>��>8Q�=��z��׾������
>�(�?��
?��H@G�@!G�@�H?�
=?��?��?xQ�?u?8Q�>�ff>�  �\��\)����>B�\?&ff?�(�?�G�@
=q@�@z�?޸R?�
=?�p�?�G�?�  ?��
?(��>\�����\)=�G�>�(�?fff?�{?�p�?�
=?��?ٙ�?\?�33?���?��?�  ?��?!G�>���                                                                    @@                                                                      ?�                                                                      ?�                                                                      ?�                                                              @   @�  @@              ?�  ?�                                  @�  A0  A�  A`  @�          ?�  @   @@  @   ?�          @   @@          @�  A`  A�  A�  A          @�  A   A  @�  ?�  @�  A  AP  Ap  @�  ?�  @�  A@  A�  A�  A   ?�  @�  AP  A�  A�  A  @@  A   A�  A�  A�  A@  @   @   @�  @�  @�  @�      A  A�  A�  A�  A�  A  A   A�  A�  A�  A`  @@              @   @�  ?�  @�  A�  A�  A�  A�  A   @   A`  A�  A`  A   @                   @   @�  @�  A@  A�  A�  A�  A   @   @�  @�                              @@  A�  A0  Ap  A�  A�  A�  A   @�  @�  @�  @�  @@  @@          @   @@  @@  A�  AP  A�  A�  A�  A�  A   @�  A   A   A   A0  A   @�  A   A0  A0  A@  A@  A@  A�  A�  A�  A�  A   AP  AP  A@  A@  A0  A�  A�  A�  A�  A�  A�  A  A0  A�  A�  A�  Ap  A   A  @�  A  Ap  A�  A�  B   A�  B   A�  B  AP  Ap  A�  A�  A�  A`  A   @�  @�  A  A�  A�  B  B  B  B  B  B  A�  A�  B  B   A�  A  @�  A   A   @�  A�  A�  B  B  B   A�  A�  A�  A�  B  B,  B  A�  A   @�  A   A@  A�  A�  B  B  B  A�  A�  A�  A�  A�  B<  BP  B  A�  A�  A�  A�  A�  A�  B  B4  B  A�  A�  B  B$  B<  B,  Bl  B\  B  A�  B   B  A�  A�  A�  B\  BT  B(  B   A�  B4  Bd  B�  B\  B�  Bh  B@  B  B  B  B  A�  A�  B(  B4  B(  B  A�  B<  B�  B�  Bt  Bt  Bd  BT  B$  A�  A�  A�  B  B  B0  B4  B4  B<  B@  Bd  B�  B�  BT  BT  B8  B  A�  A�  A�  A�  B  BD  B�  B�  BT  Bp  B�  Bl  B�  B�  B,  Bd  B�  B�  B�  B�  B�  B�  B�  B�  B�  B�  B�  B�  B�  BT  B  Ap  BX  B�  B�  B�  B�  B�  B�  B�  B�  B�  B�  B�  B�  B�  B�  Bl  B(  A�  B|  B�  B�  B�  B�  B�  B�  B�  B�  B\  BP  BT  Bl  B�  Bx  B`  B4  B   B�  B�  B�  B�  B�  B�  B�  B|  B8  B  A�  A�  B  B,  BP  B`  BH  B   B�  B�  B�  B�  B�  B�  B�  Bh  B  A�  A`  A0  AP  A�  A�  B,  BL  BT  B�  B�  B�  B�  B�  B�  B�  B�  B0  A�  A  @@  ?�  @@  A0  A�  B0  Bh  B�  B�  B�  B�  B�  B�  B�  B�  BT  A�  A@  @   ?�  ?�  @@  A   A�  B@  B�  B�  B�  B�  B�  B�  B�  B�  B�  B(  A�  AP  A  @�  @@  @   Ap  B  B�  B�  B�  B�  B�  B�  B�  B�  B�  Bh  B   A�  A�  A�  @�  @@  @�  A�  B�  B�  B�  B�  B�  B�  B�  B�  B�  B�  BX  B@  B0  B  A�  @�  @�  @�  B�  B�  B�  B�  B�  B�  B�  B�  B�  B|  Bp  B�  B|  B<  A�  @�  @   @�  B�  B�  B�  B�  B�  B�  B�  B�  Bh  B@  B\  B�  B�  BD  A�  @�  @@  @�  B�  B�  B�  B�  B�  B�  B�  B|  B$  B  BP  B�  Bl  B  A`  A   @�  A  B�  B�  B�  B�  B�  B�  B�  BX  A�  A�  B,  B@  B$  A�  A�  A�  A0  A`  B�  B�  B�  B�  B�  B�  B�  B0  A�  A�  B  B   A�  A�  A�  B   A�  A�  B�  B�  B�  B�  B�  B�  Bl  B  A�  A�  A�  A�  B   B   B  B@  A�  A�  B�  B�  B�  B�  B�  Bt  B   B   A�  A�  B   B  B\  Bp  B   BH  B  A�  B�  B�  B�  B�  Bh  B  A�  A�  A�  A�  B8  B\  Bh  B�  BD  B8  B   A�  B�  B�  B|  B@  B   A�  A�  A�  A`  B  Bl  B�  B�  Bt  B@  B   B0  B  B�  BH  B  A�  A`  A@  A0  A0  A�  B4  B�  B�  B�  Bl  B0  B  B  B  B  A�  A@  @�  A0  A�  A�  Ap  A�  Bd  B�  B�  B�  Bd  B@  BD  B(  B  A�  A@  AP  A�  A�  A�  A�  A�  B  Bx  B�  B�  B�  Bl  B�  B�  Bx  BP  B,  A�  A�  A�  A�  A�  B  B  B  Bh  B�  B�  B�  Bh  Bp  B�  B�  B�  Bd  B,  B  A�  A�  B  B  B  B  BT  B�  B�  B�  B�  Bl  B�  B�  B�  